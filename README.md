# DATA SCIENCE - University of Buenos Aires #

### DATA SCIENCE ###

This project is about detecting the best machine learning method to detect SPAM or HAM (not SPAM) emails, and getting the most important features to do this comparison 

The 'selectBestMethod' folder has the code to compare diferent machine learning methods, and select the best method to do SPAM and HAM detection. The 'bestMethod' folder has the code to test the best method for SPAM and HAM detection.

### IMPORTANT ###

Read the 'informe.pdf' file. This is the investigation paper. All this project is for an university course of Machine Learning